# $Id: Makefile,v 2.1 2013/07/05 02:54:44 demon Exp $

VERSION=2.0
PROG=	spectrogram
SRCS=	spectrogram.c sio.c fft.c hsv2rgb.c
BINDIR=	/usr/local/bin
HEADERS=sio.h fft.h hsv2rgb.h
LIBS=	fftw3 x11
PCCF!=	pkg-config --cflags ${LIBS}
PCLA!=	pkg-config --libs ${LIBS}
CFLAGS+=${PCCF}
LDADD+=	${PCLA} -lsndio
DEBUG+=	-Wall
NOMAN=
DIR=	${PROG}-${VERSION}

package:
	@mkdir ${DIR}
	@cp Makefile ${SRCS} ${HEADERS} ${DIR}
	@tar zcf ${DIR}.tar.gz ${DIR}
	@rm -rf ${DIR}

.include <bsd.prog.mk>
